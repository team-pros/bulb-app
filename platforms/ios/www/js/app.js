// We use an "Immediate Function" to initialize the application to avoid leaving anything behind in the global scope
(function () {

    /* ---------------------------------- Local Variables ---------------------------------- */
	var service = new DeviceService();
	HomeView.prototype.template = Handlebars.compile($("#home-tpl").html());
	DeviceListView.prototype.template = Handlebars.compile($("#device-list-tpl").html()); 
	DeviceView.prototype.template = Handlebars.compile($("#device-tpl").html());
	ScanView.prototype.template = Handlebars.compile($("#scan-tpl").html());
  
	service.initialize().done(function () {
        console.log("Service initialized");

		service.initialize().done(function () {
			router.addRoute('', function() {
      			$('body').html(new HomeView(service).render().$el);
  			});

  			router.addRoute('devices/:id', function(id) {
      			service.findById(parseInt(id)).done(function(device) {
          			$('body').html(new DeviceView(device).render().$el);
      			});
  			});

			router.addRoute('scan', function() {
				$('body').html(new ScanView().render().$el);
			});

  			router.start();
		});
    });

    /* --------------------------------- Event Registration -------------------------------- */

    /* ---------------------------------- Local Functions ---------------------------------- */

}()
);
