var DeviceService = function() {

    this.initialize = function() {
        // No Initialization required
        var deferred = $.Deferred();
        deferred.resolve();
	return deferred.promise();
    }

    this.findById = function(id) {
        var deferred = $.Deferred();
        var device = null;
        var l = devices.length;
        for (var i=0; i < l; i++) {
            if (devices[i].id === id) {
                device = devices[i];
                break;
            }
        }
        deferred.resolve(device);
        return deferred.promise();
    }

    this.findByName = function(searchKey) {
        var deferred = $.Deferred();
        var results = devices.filter(function(element) {
            var fullName = element.name;
            return fullName.toLowerCase().indexOf(searchKey.toLowerCase()) > -1;
        });
        deferred.resolve(results);
        return deferred.promise();
    }

	this.findAll = function() {
		var deferred = $.Deferred();
		deferred.resolve(devices);
		return deferred.promise();
	}

    var devices = [
        {"id": 1, "name": "Kitchen Bulb", "type": "bulb", "configURL": "http://192.168.2.1:1234", "ip": "192.168.2.1", "pic":"bulb.jpg" },
		{"id": 2, "name": "Over the Table Bulb", "type": "bulb", "configURL": "http://192.168.2.3:1234", "ip": "192.168.2.3", "pic": "bulb.jpg"},
		{"id": 3, "name": "Living Room Dimmer", "type": "dimmer", "configURL": "http://192.168.2.4:2345", "ip": "192.168.2.4", "pic": "dimmer.jpg"},
    ];

}
