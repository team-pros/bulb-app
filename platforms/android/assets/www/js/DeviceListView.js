var DeviceListView = function () {

    var devices;

    this.initialize = function() {
        this.$el = $('<div/>');
        this.render();
    };

    this.setDevices = function(list) {
        devices = list;
        this.render();
    }

    this.render = function() {
        this.$el.html(this.template(devices));
        return this;
    };

    this.initialize();

}
