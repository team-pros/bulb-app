var DeviceView = function(device) {

	this.initialize = function() {
		this.$el = $('<div/>');

		this.$el.on('click', '#switchOn', this.switchOnRequest);
		this.$el.on('click', '#switchOff', this.switchOffRequest);


  	};

  	this.render = function() {
		this.$el.html(this.template(device));
		return this;
	};
	
	this.switchOnRequest = function() {
		$.ajax({
  			url: 'http://localhost:4000/bulb/api/v1/on',
			type: 'PUT',
  			data: '',
			success: function(data) {
			}
		});
	};

	this.switchOffRequest = function() {
		$.ajax({
  			url: 'http://localhost:4000/bulb/api/v1/off',
			type: 'PUT',
  			data: '',
			success: function(data) {
			}
		});
	};
	

	this.initialize();

}
