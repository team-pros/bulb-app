var HomeView = function(service) {

	var deviceListView;

	this.render = function() {
    	this.$el.html(this.template());
    	$('.content', this.$el).html(deviceListView.$el);
    	return this;
	};

	this.initialize = function() {
		this.$el=$('<div/>');
		this.$el.on('keyup', '.search-key', this.findByName);
		deviceListView = new DeviceListView();
		this.findAll();
		this.render();
		console.log(this);
	};
	
	this.findByName = function() {
    	service.findByName($('.search-key').val()).done(function(devices) {
        	deviceListView.setDevices(devices);
    	});
	};

	this.findAll = function() {
		service.findAll().done(function(devices) {
			deviceListView.setDevices(devices);
		});
	}

	this.initialize();

}
